package com.sportme.fe.jpa;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pinhask on 7/27/17.
 */
@Entity
@Table(name="devices")
public class Device implements Serializable {

    @Id
    private int deviceId;
    private String deviceName;
    private String catalogNumber;
    private String DeviceType;
    private String RecommendedProgram;
    private String DeviceManufacturer;
    private String DevicePicture;

    public Device() {
    }

    public Device(int deviceId, String deviceName, String catalogNumber, String deviceType, String recommendedProgram, String deviceManufacturer, String devicePicture) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.catalogNumber = catalogNumber;
        DeviceType = deviceType;
        RecommendedProgram = recommendedProgram;
        DeviceManufacturer = deviceManufacturer;
        DevicePicture = devicePicture;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getCatalogNumber() {
        return catalogNumber;
    }

    public void setCatalogNumber(String catalogNumber) {
        this.catalogNumber = catalogNumber;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getRecommendedProgram() {
        return RecommendedProgram;
    }

    public void setRecommendedProgram(String recommendedProgram) {
        RecommendedProgram = recommendedProgram;
    }

    public String getDeviceManufacturer() {
        return DeviceManufacturer;
    }

    public void setDeviceManufacturer(String deviceManufacturer) {
        DeviceManufacturer = deviceManufacturer;
    }

    public String getDevicePicture() {
        return DevicePicture;
    }

    public void setDevicePicture(String devicePicture) {
        DevicePicture = devicePicture;
    }
}
