package com.sportme.fe.jpa;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */
@Entity
@Table(name = "Ratings" )
public class Rating implements Serializable {

    @EmbeddedId
    private RatingPK ratingId;
    private String rating;

    public Rating() {
    }
    public Rating(RatingPK rId, String rating) {
        this.ratingId = ratingId;
        this.rating = rating;
    }
    public Rating(int parkId, int userId, String rating) {
        ratingId = new RatingPK(parkId, userId);
        this.rating = rating;
    }

    public void setRatingId(RatingPK ratingId) {
        this.ratingId = ratingId;
    }

    public RatingPK getParkId() {
        return ratingId;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}

