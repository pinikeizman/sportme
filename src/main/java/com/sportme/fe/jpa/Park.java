package com.sportme.fe.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by pinhask on 7/17/17.
 */

@Entity
@Table(name = "parks")
@NamedQueries({
        @NamedQuery(name = "getPark", query = "SELECT c FROM Park c WHERE  c.parkid = :parkid")
})
public class Park implements Serializable {

    @Id
    private int parkid;
    private String parkName;
    private String latitude;
    private String longitude;
    private String parkPicture;
    private String city;
    private String supportEmail;
    private String street;
    private String aboutPark;
    @Transient
    private Boolean isFollower;
    @Transient
    private Integer userRating;
    @Transient
    private List<Integer> devicesId;
    @Transient
    private Integer numOfFollowers;
    @Transient
    private Integer numOfActivities;

    @Transient
    private Integer numOfRating;

    @Transient
    private Integer parkRating;

    public Park(List<Object> data) {

        this.parkid = ((Double)data.get(0)).intValue();
        this.parkName = String.valueOf(data.get(1));
        this.aboutPark = String.valueOf(data.get(2));
        this.longitude = String.valueOf(data.get(3));
        this.latitude = String.valueOf(data.get(4));
        this.city = String.valueOf(data.get(5));
        this.parkPicture = String.valueOf(data.get(6));
        this.supportEmail = String.valueOf(data.get(7));
        this.street = String.valueOf(data.get(8));
        this.parkRating = data.get(10) == null ? null : Math.round(((Double)data.get(10)).intValue());
        this.numOfRating = data.get(11) == null ? null : ((Double)data.get(11)).intValue();
        this.numOfActivities = data.get(12) == null ? null : ((Double)data.get(12)).intValue();
        this.userRating = data.get(14) == null ? null : ((Double)data.get(14)).intValue();
        Boolean isUserFollow = data.get(17) == null ? new Boolean(false) : new Boolean(true);
        this.isFollower = isUserFollow;
        this.numOfFollowers = data.get(19) == null ? new Integer(0) : ((Double)data.get(19)).intValue();

    }

    public String toString() {
        return parkid + " " + parkName
                + " " + latitude + " " + longitude;
    }

    public Park(int parkid, String parkName, String latitude, String longitude, String parkPicture, String city, String supportEmail, String street, String aboutPark, Boolean follower, List<Integer> devicesId, Integer numOfFollowers, Integer numOfActivities, Integer numOfRating, Integer parkRating) {
        this.parkid = parkid;
        this.parkName = parkName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.parkPicture = parkPicture;
        this.city = city;
        this.supportEmail = supportEmail;
        this.street = street;
        this.aboutPark = aboutPark;
        this.isFollower = follower;
        this.devicesId = devicesId;
        this.numOfFollowers = numOfFollowers;
        this.numOfActivities = numOfActivities;
        this.numOfRating = numOfRating;
        this.parkRating = parkRating;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }

    public Park() {
    }

    public Boolean getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(Boolean isFollower) {
        this.isFollower = isFollower;
    }

    public String getAboutPark() {
        return aboutPark;
    }

    public void setAboutPark(String aboutPark) {
        this.aboutPark = aboutPark;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public List<Integer> getDevicesId() {
        return devicesId;
    }

    public void setDevicesId(List<Integer> devicesId) {
        this.devicesId = devicesId;
    }

    public Integer getNumOfActivities() {
        return numOfActivities;
    }

    public void setNumOfActivities(Integer numOfActivities) {
        this.numOfActivities = numOfActivities;
    }

    public Integer getNumOfFollowers() {
        return numOfFollowers;
    }

    public void setNumOfFollowers(Integer numOfFollowers) {
        this.numOfFollowers = numOfFollowers;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getParkPicture() {
        return parkPicture;
    }

    public void setParkPicture(String parkPicture) {
        this.parkPicture = parkPicture;
    }

    public void setParkRating(Integer parkRating) {
        this.parkRating = parkRating;
    }

    public Integer getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(Integer numOfRating) {
        this.numOfRating = numOfRating;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }


    public int getParkid() {
        return parkid;
    }

    public void setParkid(int parkid) {
        this.parkid = parkid;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getParkRating() {
        return parkRating;
    }

    public void setparkRating(Integer rating) {
        this.parkRating = rating;
    }
}
