package com.sportme.fe.jpa;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */
@Embeddable
public class RatingPK implements Serializable {

    private int parkId;
    private int userId;

    public RatingPK(){}

    public RatingPK(int parkid, int userid) {
        this.parkId = parkid;
        this.userId = userid;
    }

    public int getParkid() {
        return parkId;
    }

    public void setParkid(int parkid) {
        this.parkId = parkid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
