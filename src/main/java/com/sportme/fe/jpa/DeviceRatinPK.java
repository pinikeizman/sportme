package com.sportme.fe.jpa;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by pinhask on 7/30/17.
 */
@Embeddable
public class DeviceRatinPK implements Serializable {

    private String deviceId;
    private String userid;

    public DeviceRatinPK(){}
    public DeviceRatinPK(String deviceId, String userid) {
        this.deviceId = deviceId;
        this.userid = userid;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
