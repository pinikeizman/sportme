package com.sportme.fe.jpa;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by pinhask on 7/31/17.
 */
@Embeddable
public class UserFollowingParksPK  implements Serializable{

    private String userId;
    private String parkId;

    public UserFollowingParksPK(){}
    public UserFollowingParksPK(String userId, String parkId) {
        this.userId = userId;
        this.parkId = parkId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }
}
