package com.sportme.fe.jpa;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by pinhask on 7/31/17.
 */
@Entity
@Table(name = "UsersFollowingParks")
public class UserFollowingParks implements Serializable {

    @EmbeddedId
    private UserFollowingParksPK pk;

    public UserFollowingParks(){}
    public UserFollowingParks(UserFollowingParksPK pk) {
        this.pk = pk;
    }

    public UserFollowingParksPK getPk() {
        return pk;
    }

    public void setPk(UserFollowingParksPK pk) {
        this.pk = pk;
    }
}
