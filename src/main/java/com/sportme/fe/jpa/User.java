package com.sportme.fe.jpa;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "test1", query = "SELECT c FROM User c")
})
public class User implements Serializable {

    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userId;
    private String userPicture;
    private String userCity;
    private String userGender;
    private String userName;
    @Id
    private String userEmail;
    private String userPassword;

    public User() {
    }

    public User(String userPicture, String userCity, String userGender, String userName, String userEmail, String userPassword) {
        this.userPicture = userPicture;
        this.userCity = userCity;
        this.userGender = userGender;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
    }

    public User(Object[] res) {
        this.userId = (Integer)res[0];
        this.userPicture = String.valueOf(res[1]);
        this.userCity = String.valueOf(res[2]);
        this.userGender = String.valueOf(res[3]);
        this.userName = String.valueOf(res[4]);
        this.userEmail = String.valueOf(res[5]);
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}


