package com.sportme.fe.jpa;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by pinhask on 7/30/17.
 */
@Entity
@Table(name = "DeviceRating")
public class DeviceRating implements Serializable {
    @EmbeddedId
    private DeviceRatinPK pk;
    private String rating;

    public DeviceRating() {
    }
    public DeviceRating(String deviceId,String userId, String rating) {
        this.pk = new DeviceRatinPK(deviceId,userId);
        this.rating = rating;
    }
    public DeviceRating(DeviceRatinPK pk, String rating) {
        this.pk = pk;
        this.rating = rating;
    }

    public DeviceRatinPK getPk() {
        return pk;
    }

    public void setPk(DeviceRatinPK pk) {
        this.pk = pk;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
