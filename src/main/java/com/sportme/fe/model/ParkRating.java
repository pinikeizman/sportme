package com.sportme.fe.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/28/17.
 */

public class ParkRating implements Serializable {

    private Integer parkId;
    private Integer numOfRating;
    private Integer rating;

    public ParkRating(){}
    public ParkRating(int parkId, int numOfRating, Integer rating) {
        this.parkId = parkId;
        this.numOfRating = numOfRating;
        this.rating = rating;
    }

    public Integer getParkId() {
        return parkId;
    }

    public void setParkId(Integer parkId) {
        this.parkId = parkId;
    }

    public Integer getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(Integer numOfRating) {
        this.numOfRating = numOfRating;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
