package com.sportme.fe.model;

        import java.util.List;

/**
 * Created by pinhask on 1/27/17.
 */
public class SolrListItem {


    private int id;
    private List<String> name;
    private List<String> description;
    private List<String> smallpic;
    private List<String> city;
    private List<String> type;
    private List<String> _lat;
    private List<String> _long;
    private Long _version_;
    private String parkRating;


    public SolrListItem(){}

    public SolrListItem(int id, List<String> name, List<String> description, List<String> smallpic, List<String> city, List<String> type, List<String> _lat, List<String> _long, Long _version_, String parkRating) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.smallpic = smallpic;
        this.city = city;
        this.type = type;
        this._lat = _lat;
        this._long = _long;
        this._version_ = _version_;
        this.parkRating = parkRating;
    }

    public String getParkRating() {
        return parkRating;
    }

    public void setParkRating(String parkRating) {
        this.parkRating = parkRating;
    }

    public Long get_version_() {
        return _version_;
    }

    public void set_version_(Long _version_) {
        this._version_ = _version_;
    }

    public String get_lat() {
        return _lat.get(0);
    }

    public void set_lat(List<String> _lat) {
        this._lat = _lat;
    }

    public String get_long() {
        return _long.get(0);
    }

    public void set_long(List<String> _long) {
        this._long = _long;
    }

    public String getType() {
        return type.get(0);
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getSmallpic() {
        return smallpic.get(0);
    }

    public void setSmallpic(List<String> smallpic) {
        this.smallpic = smallpic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name.get(0);
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public String getDescription() {
        return description.get(0);
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public String getCity() {
        return city.get(0);
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

}
