package com.sportme.fe.model;

import com.sportme.fe.jpa.Device;

import java.io.Serializable;

/**
 * Created by pinhask on 7/29/17.
 */
public class DeviceEntity extends Device implements Serializable {

    private String deviceNumber;
    private String rating;
    private String numOfRating;
    private String status;
    private String parkid;
    private String userRating;

    public DeviceEntity() {
    }


    public String getParkid() {
        return parkid;
    }

    public void setParkid(String parkid) {
        this.parkid = parkid;
    }

    public DeviceEntity(Object[] res, Object[] res1) {
        this.setDeviceId(Integer.valueOf(String.valueOf(res[0])));
        this.setDeviceName(String.valueOf(res[1]));
        this.setCatalogNumber(String.valueOf(res[2]));
        this.setDeviceType(String.valueOf(res[3]));
        this.setRecommendedProgram(String.valueOf(res[4]));
        this.setDeviceManufacturer(String.valueOf(res[5]));
        this.setDevicePicture(String.valueOf(res[6]));
        this.setStatus(String.valueOf(res[9]));
        this.setDeviceNumber(String.valueOf(res[10]));
        this.setRating(String.valueOf(res1[0]));
        this.setNumOfRating(String.valueOf(res1[1]));
    }

    public DeviceEntity(Object[] res) {

        this.setDeviceNumber(String.valueOf(res[0]));
        this.setDeviceId(Integer.valueOf(String.valueOf(res[1])));
        this.setDeviceName(String.valueOf(res[2]));
        this.setCatalogNumber(String.valueOf(res[3]));
        this.setDeviceType(String.valueOf(res[4]));
        this.setRecommendedProgram(String.valueOf(res[5]));
        this.setDeviceManufacturer(String.valueOf(res[6]));
        this.setDevicePicture(String.valueOf(res[7]));
        this.setStatus(String.valueOf(res[8]));
        this.setRating(String.valueOf(res[9]));
        this.setNumOfRating(String.valueOf(res[10]));
        this.setUserRating(String.valueOf(res[13]));


    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getNumOfRating() {
        return numOfRating;
    }

    public void setNumOfRating(String numOfRating) {
        this.numOfRating = numOfRating;
    }
}
