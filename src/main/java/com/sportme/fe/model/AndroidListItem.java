package com.sportme.fe.model;

import java.io.Serializable;

/**
 * Created by pinhask on 7/20/17.
 */
public class AndroidListItem implements Serializable {

    private int id;
    private String name;
    private String description;
    private String smallpic;
    private String city;
    private String type;
    private String _lat;
    private String _long;
    private Long _version_;
    private String parkRating;

    public  AndroidListItem(){}

    public AndroidListItem(SolrListItem[] slist){
        SolrListItem sli = slist[0];
        this.id = sli.getId();
        this.name = sli.getName();
        this.description = sli.getDescription();
        this.smallpic = sli.getSmallpic();
        this.city = sli.getCity();
        this.type = sli.getType();
        this._lat = sli.get_lat();
        this._long = sli.get_long();
        this._version_= sli.get_version_();
        this.parkRating = sli.getParkRating();
    }

    public String getParkRating() {
        return parkRating;
    }

    public void setParkRating(String parkRating) {
        this.parkRating = parkRating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSmallpic() {
        return smallpic;
    }

    public void setSmallpic(String smallpic) {
        this.smallpic = smallpic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String get_lat() {
        return _lat;
    }

    public void set_lat(String _lat) {
        this._lat = _lat;
    }

    public String get_long() {
        return _long;
    }

    public void set_long(String _long) {
        this._long = _long;
    }

    public Long get_version_() {
        return _version_;
    }

    public void set_version_(Long _version_) {
        this._version_ = _version_;
    }
}
