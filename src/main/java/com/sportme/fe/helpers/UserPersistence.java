package com.sportme.fe.helpers;

import com.sportme.fe.jpa.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

/**
 * Created by pinhask on 8/1/17.
 */
public class UserPersistence {

    public static Response getUser(String userid, String password) {
        Response response = null;
        User user = null;
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
//            Query query = em.createNativeQuery(
//                    "SELECT * FROM Users WHERE Users.UserEmail = ?1 AND Users.UserPassword = ?2");
//            query.setParameter(1, userid);
//            query.setParameter(2, password);
            //Object[] res = (Object[]) query.getSingleResult();

            user = em.find(User.class, userid);//new User(res);

            if (password.compareToIgnoreCase(user.getUserPassword()) == 0) {
                user.setUserPassword(null);
                response = Response.ok().entity(user).build();
            } else {
                response = Response.status(Response.Status.BAD_REQUEST).entity("1").build();
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = Response.serverError().build();
        }
        return response;
    }
}
