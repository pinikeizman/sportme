package com.sportme.fe.helpers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.Serializable;

/**
 * Created by pinhask on 7/20/17.
 */
public class PersistenceHelper  {

    private static final java.lang.String PERSISTENCE_UNIT_NAME = "sportmedb";
    private static EntityManagerFactory factory;

    public static EntityManager getEntitiMangager(){

        if(factory == null){
            factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return factory.createEntityManager();
    }

}
