package com.sportme.fe.helpers;

import com.sportme.fe.jpa.DeviceRatinPK;
import com.sportme.fe.jpa.DeviceRating;
import com.sportme.fe.jpa.Rating;
import com.sportme.fe.model.DeviceEntity;
import com.sportme.fe.model.ParkRating;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pinhask on 7/29/17.
 */
public class DevicesPecictence {

    public static Response getDevices(List<String> list) {

        EntityManager em = PersistenceHelper.getEntitiMangager();
        Response response = null;
        try {
            ArrayList<DeviceEntity> reslist = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                Query query = em.createNativeQuery(
                        "SELECT * FROM " +
                                "Devices JOIN  DevicesInParks ON Devices.DeviceId = DevicesInParks.DeviceId WHERE DevicesInParks.DeviceNumber = ?1");
                query.setParameter(1, list.get(i));
                Object[] res = (Object[]) query.getSingleResult();
                Query query1 = em.createNativeQuery(
                        "SELECT AVG(DeviceRating.Rating),COUNT(*) FROM " +
                                "DeviceRating WHERE DeviceRating.DeviceId = ?1");
                query.setParameter(1, list.get(i));
                Object[] res1 = (Object[]) query1.getSingleResult();
                reslist.add(new DeviceEntity(res, res1));
            }
            response = Response.ok().entity(reslist).build();

        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            em.close();
        }
        return response;

    }

    public static Object[] getDeviceRating(int deviceId) {
        ParkRating parkRating = null;
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            Query query = em.createNativeQuery(
                    "SELECT AVG(S.rating),COUNT(*) FROM DeviceRating AS S WHERE S.DeviceId = ?1");
            query.setParameter(1, deviceId);
            Object[] res = (Object[]) query.getSingleResult();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Response setUserRatingForDevice(DeviceRating rating) {
        Object [] res = null;
        EntityManager em = PersistenceHelper.getEntitiMangager();
        EntityTransaction t = em.getTransaction();
        try {
            DeviceRating dr = em.find(DeviceRating.class, rating.getPk());
            if(dr == null){
                dr = new DeviceRating(rating.getPk(), rating.getRating());
            }
            t.begin();
            dr.setRating(rating.getRating());
            em.persist(dr);
            t.commit();
            res = getDeviceRating(Integer.valueOf(rating.getPk().getDeviceId()));
        } catch (Exception e) {
            t.rollback();
            return Response.serverError().build();
        } finally {
            em.close();
        }
        return Response.ok(res).build();

    }


    public static Object[] getDeviceRatingForUser(String deviceid, String userid) {

        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            Query query = em.createNativeQuery(
                    "SELECT T.DeviceId,T.rating,numOfRating,D.Rating FROM (SELECT AVG(rating) as rating,COUNT(*) as numOfRating,DeviceId FROM DeviceRating" +
                            "WHERE DeviceId = ?1) as T JOIN" +
                            "(SELECT Rating,DeviceId From DeviceRating WHERE DeviceId = ?1 AND UserId = ?2) AS D" +
                            "ON T.DeviceId = D.DeviceId"
            );
            query.setParameter(1, deviceid);
            query.setParameter(2, userid);
            Object[] res = (Object[]) query.getSingleResult();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
