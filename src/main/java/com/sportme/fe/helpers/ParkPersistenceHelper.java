package com.sportme.fe.helpers;

import com.google.gson.Gson;
import com.sportme.fe.jpa.*;
import com.sportme.fe.model.DeviceEntity;
import com.sportme.fe.model.ParkRating;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.sportme.fe.helpers.DevicesPecictence.getDeviceRating;

/**
 * Created by pinhask on 7/20/17.
 */
public class ParkPersistenceHelper {

    static Gson gson = new Gson();

    public static ParkRating getParkRating(int parkid) {
        ParkRating parkRating = null;
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            Query query = em.createNativeQuery(
                    "SELECT AVG(S.rating),COUNT(*) FROM Ratings AS S WHERE S.parkid = ?1");
            query.setParameter(1, parkid);
            Object[] res = (Object[]) query.getSingleResult();
            BigDecimal l = res[0] != null ? (BigDecimal) res[0] : new BigDecimal(0);
            parkRating = new ParkRating(
                    parkid,
                    Math.round((Long) res[1]),
                    (int) Math.round(l.doubleValue())
            );
//    public ParkRating(String parkId, String numOfRating, String rating) {

            return parkRating;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parkRating;
    }

    public static Long getParkFollowers(int parkid) {
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            Query query = em.createNativeQuery(
                    "SELECT COUNT(*) FROM UsersFollowingParks AS S WHERE S.parkid = ?1");
            query.setParameter(1, parkid);
            Long res = (Long) query.getSingleResult();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Long(0);
    }

    public static Double getParkNumberOfActivities(int parkid) {
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            Query query = em.createNativeQuery(
                    "SELECT COUNT(*) FROM Activities AS S WHERE S.parkid = ?1");
            query.setParameter(1, parkid);
            Double res = (Double) query.getSingleResult();
            return res;
        } catch (Exception e) {

        }
        return new Double(0);
    }

    public static Object getUserRatingForPark(int userid, int parkid) {

        EntityManager em = PersistenceHelper.getEntitiMangager();
        Rating rating = null;
        try {
            rating = em.find(Rating.class, new RatingPK(parkid, userid));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return rating;

    }

    public static ParkRating setUserRatingForPark(Rating rating) {

        EntityManager em = PersistenceHelper.getEntitiMangager();
        EntityTransaction t = em.getTransaction();
        ParkRating res = null;
        try {
            t.begin();
            em.merge(rating);
            t.commit();
            res = getParkRating(rating.getParkId().getParkid());
        } catch (Exception e) {
            t.rollback();
        } finally {
            em.close();
        }
        return res;

    }

    public static Response getParkDeviceEntityAsList(String parkid) {
        EntityManager em = PersistenceHelper.getEntitiMangager();
        Response response = null;
        try {
            Query query = em.createNativeQuery(
                    "SELECT S.DeviceNumber FROM DevicesInParks AS S WHERE S.parkid = ?1");
            query.setParameter(1, parkid);
            List<String> res = (List<String>) query.getResultList();
            response = Response.ok().entity(Arrays.asList(res)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public static Response getDeviceEntityList(String parkid, String userid) {
        EntityManager em = PersistenceHelper.getEntitiMangager();
        Response response = null;
        List<DeviceEntity> res = null;
        try {
            Query query = em.createNativeQuery(
                    " SELECT * FROM (SELECT DevicesInParks.DeviceNumber,Devices.DeviceId," +
                            " Devices.DeviceName,Devices.CatalogNumber,Devices.DeviceType," +
                            " Devices.RecommendedProgram,Devices.DeviceManufacturer,Devices.DevicePicture," +
                            " DevicesInParks.DeviceStatus,D.rating,D.numOfRating FROM DevicesInParks" +
                            " JOIN Devices ON DevicesInParks.DeviceId = Devices.DeviceId" +
                            " LEFT JOIN (SELECT DeviceId,AVG(Rating) as rating,COUNT(*) as numOfRating FROM DeviceRating GROUP BY DeviceId) AS D" +
                            " ON DevicesInParks.DeviceId = D.DeviceId" +
                            " WHERE DevicesInParks.ParkId = ?1) AS T" +
                            " LEFT JOIN (SELECT * FROM DeviceRating As D WHERE D.UserId = ?2) as G" +
                            " ON T.DeviceId = G.DeviceId");
            query.setParameter(1, parkid);
            query.setParameter(2, userid);
            List<Object[]> data = (List<Object[]>) query.getResultList();
            res = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                res.add(new DeviceEntity(data.get(i)));
            }
            response = Response.ok().entity(res).build();
        } catch (Exception e) {
            return Response.serverError().build();
        } finally {
            em.close();
            return response;
        }
    }

    public static Response setPrkFollower(String parkId, String userId) {
        EntityManager em = PersistenceHelper.getEntitiMangager();
        EntityTransaction t = em.getTransaction();
        Integer res = null;
        UserFollowingParksPK pk = new UserFollowingParksPK(userId, parkId);
        try {
            UserFollowingParks dr = em.find(UserFollowingParks.class, pk);
            if (dr == null) {
                dr = new UserFollowingParks(pk);
                t.begin();
                em.persist(dr);
                t.commit();
            }
            res = getParkFollowers(Integer.valueOf(parkId)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            t.rollback();
            return Response.serverError().build();
        } finally {
            em.close();
        }
        return Response.ok(res).build();
    }

    public static Response getParkForUser(String parkid, String userid) {
        EntityManager em = PersistenceHelper.getEntitiMangager();
        Response response = null;
        Park res = null;
        try {
            Query query = em.createNativeQuery(
                   " SELECT * FROM (SELECT * FROM Parks WHERE Parks.ParkId = ?1) AS P" +
                            " LEFT JOIN  (SELECT ParkId,AVG(Rating) as parkRating,COUNT(*) as numOfRating FROM Ratings WHERE ParkId = ?1) AS R" +
                            " ON R.ParkId = P.ParkId" +
                            " LEFT JOIN (SELECT COUNT(*) AS numOfActivities,parkId FROM Activities WHERE parkId = ?1 GROUP BY parkId) AS A" +
                            " ON A.parkId = R.ParkId" +
                            " LEFT JOIN (SELECT Rating AS userParkRating,ParkId FROM Ratings WHERE ParkId = ?1 AND  UserId = ?2) AS  T" +
                            " ON T.ParkId=P.ParkId " +
                            " LEFT JOIN (SELECT ParkId,COUNT(*) as isUserFollowing FROM UsersFollowingParks WHERE ParkId = ?1 AND UserId = ?2 ) as U" +
                            " ON U.ParkId = P.ParkId"+
                            " LEFT JOIN (SELECT ParkId,COUNT(*) as numOfFollowing FROM UsersFollowingParks WHERE ParkId = ?1  GROUP BY ParkId) as O"+
                            " ON O.ParkId = P.ParkId");
            query.setParameter(1, parkid);
            query.setParameter(2, userid);
            String jsonData = gson.toJson(query.getSingleResult());
            res = new Park(gson.fromJson(jsonData, List.class));
            response = Response.ok().entity(res).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        } finally {
            em.close();
        }
        return response;
    }
}
