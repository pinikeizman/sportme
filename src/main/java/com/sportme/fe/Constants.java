package com.sportme.fe;

/**
 * Created by pinhask on 7/28/17.
 */
public interface Constants {

    java.lang.String QUERY_SET_USER_RATING_FOR_PARK = "getUserRatingForPark";
    String QUERY_PARAM_RATING_ID = "ratingid" ;
    String QUERY_PARAM_PARK_ID = "parkid" ;
    String QUERY_PARAM_USER_ID = "userid" ;

}
