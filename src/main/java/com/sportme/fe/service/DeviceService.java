package com.sportme.fe.service;

import com.google.gson.Gson;
import com.sportme.fe.helpers.DevicesPecictence;
import com.sportme.fe.jpa.DeviceRating;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by pinhask on 8/1/17.
 */
@Path("/rest")
public class DeviceService {
    private Gson gson = new Gson();

    @GET
    @Path("/devices/{deviceid}/rating")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getDevicekRating(@PathParam("deviceid") int deviceid) {
        Response response = null;
        Object[] res = null;
        try {
            res = DevicesPecictence.getDeviceRating(deviceid);
        } catch (Exception e) {
            e.printStackTrace();
            response = Response.serverError().build();
        }
        return gson.toJson(res);
    }

    @GET
    @Path("/devices/{deviceid}/rating/users/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getDeviceRatingForUser(@PathParam("deviceid") String deviceid,
                                         @PathParam("deviceid") String userid) {
        Response response = null;
        Object[] res = null;
        try {
            res = DevicesPecictence.getDeviceRatingForUser(deviceid, userid);
        } catch (Exception e) {
            e.printStackTrace();
            response = Response.serverError().build();
        }
        return gson.toJson(res);
    }

    @POST
    @Path("devices/{deviceid}/rating/users/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String SetUserRatingForDevice(@PathParam("deviceid") String deviceid, @PathParam("userid") String userid
            , String rating) {
        Response res = null;
        try {
            res = DevicesPecictence.setUserRatingForDevice(new DeviceRating(deviceid, userid, rating));
        } catch (Exception e) {
            e.printStackTrace();
            res = Response.serverError().build();
        }
        return gson.toJson(res.getEntity());
    }


}
