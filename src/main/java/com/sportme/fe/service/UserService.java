package com.sportme.fe.service;

import com.google.gson.Gson;
import com.sportme.fe.helpers.UserPersistence;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.MessageDigest;
import java.util.Map;

/**
 * Created by pinhask on 8/1/17.
 */
@Path("/rest")
public class UserService {
    private Gson gson = new Gson();

    @POST
    @Path("/users/login")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String loginUser(String _data) {
        Map<String,String> data = gson.fromJson(_data,Map.class);
        String userid=data.get("userEmail"), password =data.get("userPassword");
        try {
            MessageDigest digester = MessageDigest.getInstance("SHA-256");
            digester.update(password.getBytes());
            byte[] digest = digester.digest();
            String passInHex = String.format("%064x", new java.math.BigInteger(1, digest));

            Response res = UserPersistence.getUser(userid,passInHex);
            if(res.getStatus() == 200) {
                return gson.toJson(res.getEntity());
            }else if(res.getStatus() == 404){
                return gson.toJson(res.getEntity());
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
