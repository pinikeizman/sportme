package com.sportme.fe.service;

import com.google.gson.Gson;
import com.sportme.fe.helpers.ParkPersistenceHelper;
import com.sportme.fe.model.ParkRating;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Path("/search")
public class SolrService {

    @GET
    @Path("entity/{query}")
    @Produces({"application/json"})
    public String getSreachResult(@PathParam("query") String query) {
        try {
                Client client = ClientBuilder.newClient();
            WebTarget solrServer = client.target("http://localhost:8983/solr/viewEntities")
                    .path("select").queryParam("wt", new Object[]{"json"}).queryParam("indent", new Object[]{"on"}).queryParam("q", new Object[]{query});
            String jsonRes = (String) solrServer.request(new String[]{"application/json"}).get(String.class);
            Map jsonJavaRootObject = (Map) new Gson().fromJson(jsonRes, Map.class);
            Gson gson = new Gson();
            ArrayList<Map> solrEntityList = (ArrayList<Map>) ((Map) jsonJavaRootObject.get("response")).get("docs");
            for (int i = 0; i < solrEntityList.size(); i++) {
                ParkRating res =ParkPersistenceHelper.getParkRating(Integer.parseInt(solrEntityList.get(i).get("id").toString()));
                solrEntityList.get(i).put("parkRating", String.valueOf(res.getRating()));
                solrEntityList.get(i).put("numOfUsersRating", String.valueOf(res.getNumOfRating()));
            }

            return gson.toJson(solrEntityList);

        } catch (Exception e) {

            String _e = e.toString();
        }

        return "";
    }

}