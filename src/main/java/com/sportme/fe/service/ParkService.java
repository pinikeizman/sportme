package com.sportme.fe.service;
import com.google.gson.Gson;
import com.sportme.fe.helpers.ParkPersistenceHelper;
import com.sportme.fe.helpers.PersistenceHelper;
import com.sportme.fe.jpa.Park;
import com.sportme.fe.jpa.Rating;
import com.sportme.fe.model.DeviceEntity;
import com.sportme.fe.model.ParkRating;

import javax.naming.NamingException;
import javax.persistence.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/rest")
public class ParkService {

    Gson gson = new Gson();


    @GET
    @Path("/parks/{parkid}/devices/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getParkDevicesEntity(@PathParam("parkid") String parkid, @PathParam("userid") String userid) {
        ArrayList<DeviceEntity> reslist = null;
        try {
            Response response = ParkPersistenceHelper.getDeviceEntityList(parkid, userid);
            if (response.getStatus() == 200) {
                //int deviceId, String deviceName, String catalogNumber, String deviceType, String recommendedProgram, String deviceManufacturer, String devicePicture, String deiceNumber, String rating, String numOfRating) {
                reslist = (ArrayList<DeviceEntity>) response.getEntity();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gson.toJson(reslist);
    }

    @GET
    @Path("/parks/{parkid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getPark(@PathParam("parkid") String parkid) throws NamingException {
        Park park = null;
        try {
            EntityManager em = PersistenceHelper.getEntitiMangager();
            park = (Park) em.createNamedQuery("getPark").setParameter("parkid", Integer.parseInt(parkid)).getSingleResult();
            //Get park rating
            ParkRating pr = ParkPersistenceHelper.getParkRating(park.getParkid());
            park.setparkRating(pr.getRating());
            park.setNumOfRating(Integer.valueOf(pr.getNumOfRating()));
            //Get Park followers
            park.setNumOfFollowers(ParkPersistenceHelper.getParkFollowers(park.getParkid()).intValue());
            //Get Activities
            park.setNumOfActivities(ParkPersistenceHelper.getParkNumberOfActivities(park.getParkid()).intValue());
            //Get DeivicesId list
            List<Integer> list = (List<Integer>) ParkPersistenceHelper.getParkDeviceEntityAsList(String.valueOf(park.getParkid())).getEntity();
            park.setDevicesId(list);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Gson().toJson(park);
    }

    @GET
    @Path("/parks/{parkid}/users/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getParkForUsr(@PathParam("parkid") String parkid, @PathParam("userid") String userid) throws NamingException {
        Park park = null;
        try {
            Response response = ParkPersistenceHelper.getParkForUser(parkid, userid);
            if (response.getStatus() == 200) {
                park = (Park) response.getEntity();
                List<Integer> list = (List<Integer>) ParkPersistenceHelper.getParkDeviceEntityAsList(String.valueOf(park.getParkid())).getEntity();
                park.setDevicesId(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Gson().toJson(park);
    }

    @GET
    @Path("/parks/rating/{parkid}/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getUserRatingForPark(@PathParam("parkid") int parkid,
                                       @PathParam("userid") int userid) {
        try {
            Rating res = (Rating) ParkPersistenceHelper.getUserRatingForPark(userid, parkid);
            return gson.toJson(res, Rating.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @POST
    @Path("/parks/rating/{parkid}/{userid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response setUserRatingForPark(String rating) {
        ParkRating res = null;
        Response response = null;
        try {
            res = ParkPersistenceHelper.setUserRatingForPark(gson.fromJson(rating, Rating.class));
            response = Response.ok().entity(res).build();

        } catch (Exception e) {
            e.printStackTrace();
            response = Response.serverError().build();
        }

        return response;
    }

    @POST
    @Path("/parks/{parkid}/follow")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String setParkFollower(@PathParam("parkid") String parkId, String userId) {
        try {
            Response response = ParkPersistenceHelper.setPrkFollower(parkId, userId);
            return String.valueOf(response.getEntity());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @GET
    @Path("/parks/rating/{parkid}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public ParkRating getParkRating(@PathParam("parkid") int parkid) {
        Response response = null;
        try {

            ParkRating res = ParkPersistenceHelper.getParkRating(parkid);
            if (res != null) {
                response = Response.ok().entity(res).build();
            } else {
                response = Response.serverError().build();
            }

        } catch (Exception e) {
            e.printStackTrace();
            response = Response.serverError().build();
        }
        return (ParkRating) response.getEntity();
    }


}
